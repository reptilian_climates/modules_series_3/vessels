
'''
	python -m http.server --bind 0.0.0.0 80
	
	python -m http.server 8000
	python -m http.server 8001
'''


def add_to_system_paths (trails):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()
	for trail in trails:
		sys.path.insert (0, normpath (join (this_directory, trail)))

add_to_system_paths ([ 
	'../../structures',
	'../../structures_pip'
])

# config_path = str (normpath (join (this_directory, "haproxy.conf")))
config_path = "/etc/haproxy/haproxy.cfg"

import vessels.proxies.HA.configs.HTTP_balancer as HA_HTTP_balancer
HA_HTTP_balancer.build (
	config_path = "/etc/haproxy/haproxy.cfg",

	from_port = "80",
	to_addresses = [
		"0.0.0.0:8000",
		"0.0.0.0:8001"
	]
)


import os
os.system (f"haproxy -f '{ config_path }' -c")
os.system ("systemctl restart haproxy")
os.system (f"cat '{ config_path }'")
os.system ("systemctl status haproxy -l --no-pager")


import vessels.proxies.HA as HA_Proxy
HA_Proxy.start ()