
'''
	(cd /etc/haproxy/SSL && rm -rf *)
'''

'''
	#
	# as a super duper user
	#
	(cd /etc/haproxy/SSL && openssl req -x509 -newkey rsa:4096 -keyout certificate.pem.key -out certificate.pem -sha256 -days 20000 -nodes -subj "/C=/ST=/L=/O=/OU=/CN=")
'''


def add_to_system_paths (trails):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()
	for trail in trails:
		sys.path.insert (0, normpath (join (this_directory, trail)))

add_to_system_paths ([ 
	'../../../structures',
	'../../../structures_pip'
])


import vessels.proxies.HA.SSL as HA_SSL
HA_SSL.build_papers (
	certificate_path = "/etc/haproxy/SSL/certificate.pem",
	key_path = "/etc/haproxy/SSL/certificate.pem.key"
)

import vessels.proxies.HA.configs.HTTPS_to_HTTP as HA_HTTPS_to_HTTP
HA_HTTPS_to_HTTP.build (
	start = "yes",

	SSL_certificate_path = "/etc/haproxy/SSL/certificate.pem",
	config_path = "/etc/haproxy/haproxy.cfg",
	
	to_addresses = [
		"0.0.0.0:8000",
		"0.0.0.0:8000",
		"0.0.0.0:8000",
		"0.0.0.0:8000",
		"0.0.0.0:8000",
		"0.0.0.0:8000",
		"0.0.0.0:8000",
		"0.0.0.0:8000"
	]
)
