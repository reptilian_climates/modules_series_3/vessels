
'''
	python -m http.server --bind 0.0.0.0 80
	
	python -m http.server 8000
	python -m http.server 8001
'''

'''
	systemctl cat haproxy.service
		# /usr/lib/systemd/system/haproxy.service
			
		systemctl daemon-reload
'''	

'''
	systemctl edit haproxy.service
	systemctl daemon-reload
	
	nano /etc/systemd/system/nginx.service.d
	nano /etc/systemd/system/haproxy.service.d
'''

'''
	/usr/sbin/haproxy -Ws -f /etc/haproxy/haproxy.cfg -f /etc/haproxy/conf.d -p /run/haproxy.pid
	haproxy -V -f /etc/haproxy/haproxy.cfg
	
	
	This is the command that is failing.
		haproxy -f /etc/haproxy/haproxy.cfg -f $CFGDIR -c -q $OPTIONS

		haproxy -f /etc/haproxy/haproxy.cfg -f /etc/haproxy/conf.d -c -q $OPTIONS
		
		
	cat /usr/lib/systemd/system/service.d/10-timeout-abort.conf
'''

'''
	clear logs?
		journalctl --rotate
		journalctl --vacuum-time=1s

		retain past 2 days:
			journalctl --vacuum-time=2d
'''

'''
	journalctl -u haproxy.service

	 journalctl -u haproxy.service --since today --no-pager
'''

'''
	systemctl status haproxy.service
	journalctl -xeu haproxy.service

	haproxy -c -V -f /etc/haproxy/haproxy.cfg
'''

def add_to_system_paths (trails):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()
	for trail in trails:
		sys.path.insert (0, normpath (join (this_directory, trail)))

add_to_system_paths ([ 
	'../../structures',
	'../../structures_pip'
])

import pathlib
from os.path import dirname, join, normpath
this_directory = pathlib.Path (__file__).parent.resolve ()
# config_path = str (normpath (join (this_directory, "haproxy.conf")))
config_path = "/etc/haproxy/haproxy.cfg"
SSL_certificate_path = str (normpath (join (this_directory, "certificate.pem")))


import vessels.proxies.HA.configs.HTTPS_to_HTTP as HA_HTTPS_to_HTTP
HA_HTTPS_to_HTTP.build (
	SSL_certificate_path = "/etc/haproxy/SSL/certificate.pem",
	config_path = "/etc/haproxy/haproxy.cfg",
	
	to_addresses = [
		"0.0.0.0:8000",
		"0.0.0.0:8001"
	]
)

import os
os.system (f"haproxy -f '{ config_path }' -c")
os.system ("systemctl restart haproxy")
os.system (f"cat '{ config_path }'")
os.system ("systemctl status haproxy -l --no-pager")


import vessels.proxies.HA as HA_Proxy
HA_Proxy.start ()