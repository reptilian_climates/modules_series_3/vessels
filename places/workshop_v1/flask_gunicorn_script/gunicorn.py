
def add_to_system_paths (trails):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()
	for trail in trails:
		sys.path.insert (0, normpath (join (this_directory, trail)))

add_to_system_paths ([ 
	'../../structures',
	'../../structures_pip'
])



def start ():
	from flask import Flask

	app = Flask (__name__)

	@app.route ("/")
	def hello_world ():
		return "<p>Hello, World!</p>"
		
	return app;

from gunicorn.app.base import Application
class FlaskApplication (Application):
	def init(self, parser, opts, args):
		return {
			#'bind': '{0}:{1}'.format (host, port),
			'workers': 4
		}

	def load (self):
		return start ()

application = FlaskApplication ()
application.run ()