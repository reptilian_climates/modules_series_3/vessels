



def add_to_system_paths (trails):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()
	for trail in trails:
		sys.path.insert (0, normpath (join (this_directory, trail)))

add_to_system_paths ([ 
	'../structures',
	'../structures_pip'
])

import vessels.SSH.send as SSH_send
	
from os.path import dirname, join, normpath
import pathlib
import sys

this_directory = pathlib.Path (__file__).parent.resolve ()
from_directory = normpath (join (this_directory, "../platform"))

script = SSH_send.splendidly ({
	"private key": "/RSA.private",
	
	"from": {
		"directory": "/from_here",
		"exclude": [
			"dir_1"
		]
	},
	
	"to": {
		"address": "###.###.###.###",
		"directory": "/platform"
	},
	
	"return script": "yes"
})

print ("script:", script)